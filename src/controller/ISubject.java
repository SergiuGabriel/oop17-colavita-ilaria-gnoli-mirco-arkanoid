package controller;

/**
 * Soggetto da implementare che emette le notifiche a tutti i suoi osservatori.
 */
public interface ISubject {
    /**
     * Metodo per aggiungere un osservatore.
     * @param obs - osservatore
     */
    void addObserver(IObserver obs);
}
