package utility;

import java.util.Optional;

import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import model.ModelCostant;
import model.entities.Ball;
import model.entities.Brick;
import model.entities.Entity;
import model.entities.PowerUp;
import model.entities.Projectile;
import view.utils.ImageViewObject;
import model.entities.Bar;

/**
 * Classe utile a trasformare gli {@link Entity} del model in oggetti rappresentabili su schermo.
 */
public final class Mapper {

    private Mapper() { }

    /**
     * @param ent - {@link Entity} da rappresentare con un oggetto utilizzabile dalla view.
     * @return Optional(Shape)
     */
    public static Optional<Shape> entityToView(final Entity ent) {
        if (ent instanceof Ball) {
            return Optional.of(entityToView((Ball) ent));
        } else if (ent instanceof Brick) {
            return Optional.of(entityToView((Brick) ent)); 
        } else if (ent instanceof Bar) {
            return Optional.of(entityToView((Bar) ent)); 
        } else if (ent instanceof PowerUp) {
            return Optional.of(entityToView((PowerUp) ent));
        } else if (ent instanceof Projectile) {
            return Optional.of(entityToView((Projectile) ent));
        }

        return Optional.empty();
    }

    /**
     * Metodo per rappresentare una {@link Ball}.
     * 
     * @param ball
     * @return {@link Circle}
     */
    private static Circle entityToView(final Ball ball) {
        switch (ball.getType()) { //in previsione di altre tipologie di PowerUp per la pallina
        case FIRE_BALL:
            return new Circle(ball.getPosition().getKey(), ball.getPosition().getValue(), ball.getRadius(), Color.RED);
        default:
            return new Circle(ball.getPosition().getKey(), ball.getPosition().getValue(), ball.getRadius(), Color.MEDIUMORCHID);
        }
    }

    /**
     * Get the representation of a {@link Brick}.
     * 
     * @param brick
     * @return {@link Rectangle}
     */
    private static Rectangle entityToView(final Brick brick) {
        final Rectangle r = new Rectangle(brick.getWidth(), brick.getHeight());
        r.setTranslateX(brick.getMinX());
        r.setTranslateY(brick.getMinY());
        r.setFill(brick.getBrickColor());
        r.setArcHeight(ModelCostant.DEFAULT_ARC);
        r.setArcWidth(ModelCostant.DEFAULT_ARC);
        //        r.setOpacity(0.9);
        return r;
    }

    /**
     * Get the rapresentation of a {@link Bar}.
     * 
     * @param bar
     * @return {@link Rectangle}
     */
    private static Rectangle entityToView(final Bar bar) {
        final Rectangle rect = new Rectangle();
        rect.setX(bar.getMinX());
        rect.setY(bar.getMinY());
        rect.setWidth(bar.getMaxX() - bar.getMinX());
        rect.setHeight(bar.getMaxY() - bar.getMinY());
        rect.setFill(Color.MEDIUMORCHID);
        rect.setArcHeight(ModelCostant.DEFAULT_ARC);
        rect.setArcWidth(ModelCostant.DEFAULT_ARC);

        return rect;
    }

    /**
     * Get the rapresentation of a {@link Projectile}.
     * 
     * @param projectile
     * @return {@link Rectangle}
     */
    private static Rectangle entityToView(final Projectile projectile) {
        final Rectangle r = new Rectangle(projectile.getMinX(), projectile.getMinY(), projectile.getWidth(), projectile.getHeight());
        r.setArcHeight(ModelCostant.DEFAULT_ARC);
        r.setArcWidth(ModelCostant.DEFAULT_ARC);
        r.setFill(Color.RED);

        return r;
    }

    /**
     * Metodo per rappresentare un {@link PowerUp}.
     * 
     * @param powerUp
     * @return {@link Ellipse}
     */
    private static Ellipse entityToView(final PowerUp pow) {
        final Ellipse ell = new Ellipse();

        ell.setCenterX(pow.getMinX() + (pow.getMaxX() - pow.getMinX()) / 2);
        ell.setCenterY(pow.getMinY() + (pow.getMaxY() - pow.getMinY()) / 2);
        ell.setRadiusX((pow.getMaxX() - pow.getMinX()) / 2);
        ell.setRadiusY((pow.getMaxY() - pow.getMinY()) / 2);

        ImagePattern image = null;
        switch (pow.getType()) {
        case BIG_BAR:
            image = new ImagePattern(ImageViewObject.BIG_BAR_P.getImage());
            break;
        case FIRE_BALL:
            image = new ImagePattern(ImageViewObject.FIRE_BALL_P.getImage());
            break;
        case INCREASE_LIVES:
            image = new ImagePattern(ImageViewObject.MORE_LIFE_P.getImage());
            break;
        case LASER_BAR:
            image = new ImagePattern(ImageViewObject.LASER_BAR_P.getImage());
            break;
        case LITTLE_BAR:
            image = new ImagePattern(ImageViewObject.LITTLE_BAR_P.getImage());
            break;
        case MULTIPLE_BALL:
            image = new ImagePattern(ImageViewObject.MULTIPLE_BALL_P.getImage());
            break;
        default:
            image = new ImagePattern(ImageViewObject.DEFAULT_P.getImage());
        }

        ell.setFill(image);
        return ell;
    }
}
