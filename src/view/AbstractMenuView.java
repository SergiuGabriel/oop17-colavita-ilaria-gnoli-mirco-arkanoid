package view;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import view.utils.ImageViewObject;

/**
 * Classe astratta utile a modellare tutte le scene (non giocabili) accessibili dal men� principale.
 */
//Credits - Highscore
public abstract class AbstractMenuView extends Scene implements SceneMenu {

    private final Stage stage;
    private static final Font TITLE_FONT = new Font("Algerian", 30);
    private static final Insets TITLE_PADDING = new Insets(14);
    private static final Font BACK_BUTTON_FONT = new Font("Serif", 20);

    /**
     * @param stage - Stage in cui disegnare la scena.
     */
    public AbstractMenuView(final Stage stage) {
        super(new BorderPane());

        this.stage = stage;
        this.stage.setTitle(getTitle());

        final BorderPane mainPane = (BorderPane) this.getRoot();
        final BackgroundImage bgimg = new BackgroundImage(ImageViewObject.SFONDO_MENU.getImage(),  BackgroundRepeat.NO_REPEAT, BackgroundRepeat.ROUND, BackgroundPosition.CENTER,  new BackgroundSize(BackgroundSize.AUTO, BackgroundSize.AUTO, false, false, true, true));
        mainPane.setBackground(new Background(bgimg));

        mainPane.setCenter(centerPane());
        mainPane.setBottom(bottomPane());
        mainPane.setTop(topPane());
    }

    @Override
    public abstract Node centerPane();

    /**
     * {@inheritDoc}
     */
    @Override
    public final Node topPane() {
        final FlowPane p = new FlowPane();
        p.setAlignment(Pos.CENTER);

        final Label title = new Label(getTitle());
        title.setFont(TITLE_FONT);
        title.setTextFill(Color.RED);
        title.setPadding(TITLE_PADDING);

        p.getChildren().add(title);

        return p;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final Node bottomPane() {
        final FlowPane p = new FlowPane();
        p.setPadding(TITLE_PADDING);
        final Button b = new Button("Back to menu'");
        b.setFont(BACK_BUTTON_FONT);
        b.setContentDisplay(ContentDisplay.RIGHT);
        final ImageView image = new ImageView(ImageViewObject.BACK_ICON.getImage());
        image.setFitHeight(TITLE_FONT.getSize());
        image.setPreserveRatio(true);
        b.setGraphic(image);

        b.setOnAction(e -> {
            this.stage.setScene(new DefaultScene(this.stage));
        });

        p.setAlignment(Pos.CENTER_RIGHT);
        p.getChildren().add(b);

        return p;
    }

    @Override
    public abstract String getTitle();

    /**
     * Metodo per ottenere lo stage.
     * @return Stage
     */
    protected Stage getStage() {
        return this.stage;
    }
}
