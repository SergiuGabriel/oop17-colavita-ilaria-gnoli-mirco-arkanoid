package view;

import javafx.scene.Scene;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import view.utils.ImageViewObject;

/**
 * Questa classe si occupa di fornire la schermata di visualizzazione del gioco e catturare i comandi impartiti dall'utente.
 * Il pannello centrale è un {@link ArenaView} mentre in fondo � presente un {@link ScoreAndLivesPanel}.
 */
public class GameView extends Stage {

    /**
     * 
     */
    public GameView() {
        this.setTitle("PrimaryStage");

        final BorderPane layout = new BorderPane();

        layout.setCenter(new ArenaView());
        layout.setBottom(new ScoreAndLivesPanel());
        layout.setRight(lateralBorder());
        layout.setLeft(lateralBorder());
        layout.setTop(horizontalBorder());

        final Scene scene = new Scene(layout);
        this.setScene(scene);

        this.setResizable(false);
        this.sizeToScene(); //con setResizable aggiunge  dei pixel extra. in questo modo torna alla dimensione originale
        this.show();

        this.setOnCloseRequest(e -> {
            System.exit(0);
        });
    }

    /**
     * @return {@link ArenaView}
     */
    public ArenaView getArena() {
        return (ArenaView) ((BorderPane) (this.getScene().getRoot())).getCenter();
    }

    /**
     * @return {@link ScoreAndLivesPanel}
     */
    public ScoreAndLivesPanel getInfoPanel() {
        return (ScoreAndLivesPanel) ((BorderPane) (this.getScene().getRoot())).getBottom();
    }
    /**
     * Creazione del pannello laterale con l'immagine del muro.
     * @return {@link Pane}
     */
    private Pane lateralBorder() {
        final Pane p = new Pane();
        final BackgroundImage bgimg = new BackgroundImage(ImageViewObject.LATERAL_WALL.getImage(),  BackgroundRepeat.NO_REPEAT, BackgroundRepeat.REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT);
        p.setBackground(new Background(bgimg));
        p.setPrefWidth(ImageViewObject.LATERAL_WALL.getImage().getWidth());
        return p;
    }

    /**
     * Creazione del pannello superiore con l'immagine del muro.
     * @return {@link Pane}
     */
    private Pane horizontalBorder() {
        final Pane p = new Pane();
        final BackgroundImage bgimg = new BackgroundImage(ImageViewObject.HORIZONTAL_WALL.getImage(),  BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT);
        p.setBackground(new Background(bgimg));
        p.setPrefHeight(ImageViewObject.HORIZONTAL_WALL.getImage().getHeight());
        return p;
    }
}
